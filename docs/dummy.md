# TODO

Start a local `mkdocs` instance (on port 8001 here to prevent conflict with possible other instances):

```bash
mkdocs serve --dev-addr=0.0.0.0:8001
```

To extract the YAML or JSON event log:

```bash
opentf-ctl get workflow {workflow_id} -o yaml > {filename}
```

To add a workflow log event:

```bash
python opentf/tools/generate.py --load {filename} --folder soapui/ok --overwrite
```

The `--overwrite` option is only needed if you want to use a folder that already contains an
event log.

To remove a workflow log event:

```bash
python opentf/tools/generate.py --unload --folder=soapui/ok
```
