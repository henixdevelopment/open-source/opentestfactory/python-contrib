# Copyright (c) 2021-2023 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from typing import Optional, Union

import argparse
import csv
import logging
import os
import sys

from collections import defaultdict
from datetime import datetime, timedelta

import yaml


KINDS_EVENTS = defaultdict(list)
IDS_EVENTS = defaultdict(list)

REFERENCED_EVENTS = set()

GENERATED_JOBS = {}

VERSION = '0.1.0'

INDENT = '    '

MAX_REQUESTS = 5
MAX_CONSOLE_LINES = 100
MAX_HEADER_LEN = 80


########################################################################


def touch(event) -> None:
    """Mark event as processed."""
    REFERENCED_EVENTS.add(id(event))


def as_datetime(event) -> datetime:
    return datetime.fromisoformat(event['metadata']['creationTimestamp'])


def format_timedelta(duration: Union[timedelta, str]) -> str:
    if isinstance(duration, str):
        return f'`{duration}`'
    seconds = duration.total_seconds()
    ms = int(seconds * 1000) % 1000
    days = int(seconds) // 86400
    hours = int(seconds) // 3600 % 24
    minutes = int(seconds) // 60 % 60
    seconds = int(seconds) % 60
    if days:
        return f'`{days}d{hours:02}h{minutes:02}m{seconds:02}.{ms:03}s`'
    if hours:
        return f'`{hours}h{minutes:02}m{seconds:02}.{ms:03}s`'
    if minutes:
        return f'`{minutes}m{seconds:02}.{ms:03}s`'
    return f'`{seconds}.{ms:03}s`'


def emit(msg='', file=None) -> None:
    print(msg, file=file)


def emit_yaml(item, file=None) -> None:
    """Emit item as yaml block."""
    emit('```yaml', file=file)
    emit(yaml.dump(item), file=file)
    emit('```', file=file)


def emit_block(language: str, lines, indent: str = '', file=None) -> None:
    """Emit lines as language block, with optional indent."""
    emit(f'{indent}```{language}', file=file)
    for line in lines:
        emit(f'{indent}{line}', file=file)
    emit(f'{indent}```', file=file)


def emit_collapsed_yaml(
    title: str, status: str, event, indent: str = '', file=None
) -> None:
    """Emit event as collapsed yaml block, with optional indent."""
    emit(f'{indent}??? {status} "{title}"', file=file)
    emit_block('yaml', yaml.dump(event).splitlines(), indent + INDENT, file=file)


def maybe_emit(name: str, item, indent: str = '', file=None) -> None:
    what = item.get(name)
    if what:
        emit(f'{indent}`{name}`: {what}', file=file)
        emit(file=file)


def format_tags(what) -> str:
    if isinstance(what, str):
        return f'`{what}`'
    return ' '.join(sorted(f'`{tag}`' for tag in what))


def format_title(msg: str) -> str:
    msg = ' '.join(msg.split())
    return msg[:MAX_HEADER_LEN].replace('"', '&#34;').replace('\n', ' ') + (
        '' if len(msg) < MAX_HEADER_LEN else '[...]'
    )


########################################################################
# filters


def is_workflow_notification(event) -> bool:
    return 'job_id' not in event['metadata'] and 'step_id' not in event['metadata']


def is_log_notification(event) -> bool:
    return 'logs' in event['spec']


def is_accesslog(event) -> bool:
    if event['metadata']['name'] == 'log notification':
        if 'logs' in event['spec'] and len(event['spec']['logs']) == 1:
            log = event['spec']['logs'][0]
            return '] INFO in' in log and 'x HTTP/1.1" 20' in log
    return False


def is_worker_notification(notification) -> bool:
    return 'worker' in notification['spec']


########################################################################


def emit_steps_details(steps, origin, indent='', file=None) -> None:
    for index, step in enumerate(steps, 1):
        emit_step_details(step, origin, index, indent, file=file)


def emit_steps_sumary(steps, origin, job_id, indent='', folder=None, file=None):
    durations = []
    if origin:
        print('!!! emit_steps_summary called for a non-top-level group')
    for index, step in enumerate(steps, 1):
        emit_step_summary(
            step, index, job_id, durations, indent, folder=folder, file=file
        )
        if not origin:
            generate_step(index, step, job_id, folder)
    return durations


def emit_step_summary(
    step, index, job_id, durations, indent='', folder=None, file=None
):
    step_id = step.get('id')
    if step_id is None:
        step_id = maybe_find_step_id(index, job_id)
    if 'run' in step:
        what = step['run']
    else:
        what = step['uses']
    if step_id:
        commands = [
            event
            for event in KINDS_EVENTS['ExecutionCommand']
            if (
                step_id in event['metadata']['step_origin']
                or event['metadata']['step_id'] == step_id
            )
            and event['metadata']['step_sequence_id'] != -2
        ]
        results = [
            event
            for event in KINDS_EVENTS['ExecutionResult']
            if (
                step_id in event['metadata']['step_origin']
                or event['metadata']['step_id'] == step_id
            )
            and event['metadata']['step_sequence_id'] != -2
        ]

        if commands and results:
            elapsed = max(as_datetime(event) for event in results) - min(
                as_datetime(event) for event in commands
            )
            used = timedelta(seconds=0)
            for event in results:
                used += as_datetime(event) - as_datetime(
                    [
                        cmd
                        for cmd in commands
                        if cmd['metadata']['step_sequence_id']
                        == event['metadata']['step_sequence_id']
                    ].pop()
                )
        else:
            elapsed = '???'
            used = '???'
    else:
        elapsed = '???'
        used = '???'
    durations.append([elapsed, used])
    status = find_step_status(step_id)
    emit(
        f'{indent}??? {status} "{index:0>3} {format_title(what)} [(...)]({job_id}_{index:0>3}.md "View step details") {format_timedelta(elapsed)}"',
        file=file,
    )
    emit(
        f'{indent + INDENT}Execution time on execution environment: {format_timedelta(used)}',
        file=file,
    )
    emit(
        f'{indent + INDENT}(Orchestrator overhead: {format_timedelta(elapsed - used)})',
        file=file,
    )
    emit(file=file)

    maybe_emit('name', step, indent + INDENT, file=file)
    maybe_emit('id', step, indent + INDENT, file=file)
    maybe_emit('if', step, indent + INDENT, file=file)
    if 'with' in step:
        for input in step['with']:
            emit(f'{indent}    - `{input}`', file=file)
    maybe_emit('working-directory', step, indent + INDENT, file=file)
    maybe_emit('continue-on-error', step, indent + INDENT, file=file)
    if step_id:
        emit_step_console_logs(step_id, index, job_id, indent, folder=folder, file=file)
    emit_attachments_table(step_id, indent + INDENT, file=file)


def emit_step_console_logs(
    step_id: str, index: int, job_id: str, indent: str, folder=None, file=None
) -> None:
    console_log = f'{job_id}_{index:0>3}_console.txt'
    with open(f'docs/workflows/{folder}/{console_log}', 'w', encoding='utf-8') as log:
        lines = 0
        for event in KINDS_EVENTS['ExecutionResult']:
            if not (metadata := event.get('metadata')):
                continue
            if metadata.get('step_sequence_id') == -2:
                continue
            if step_id == metadata.get('step_id') or (
                metadata.get('step_origin') and metadata['step_origin'][0] == step_id
            ):
                touch(event)
                if not (logs := event.get('logs')):
                    continue
                print(os.linesep.join(logs), file=log)
                if lines < MAX_CONSOLE_LINES:
                    emit_block(
                        'text',
                        logs[: 1 + min(MAX_CONSOLE_LINES, lines + len(logs)) - lines],
                        indent + INDENT,
                        file,
                    )
                lines += len(logs)
    if lines >= MAX_CONSOLE_LINES:
        emit(
            f'{indent+INDENT}[({lines-MAX_CONSOLE_LINES} lines omitted, click here to see full step console log)]({console_log})',
            file=file,
        )


def find_step_status(step_id: str) -> str:
    notifications = [
        event
        for event in KINDS_EVENTS['Notification']
        if event['metadata'].get('step_id') == step_id and 'conclusion' in event['spec']
    ]
    if not notifications:
        print('!!!', step_id)
        return 'success' if step_id and step_id in IDS_EVENTS else 'quote'
    touch(notifications[0])
    status = notifications[0]['spec']['conclusion']
    if status == 'skipped':
        status = 'quote'
    return status


def emit_attachments_table(step_id: str, indent: str = '', file=None) -> None:
    attachments = []
    for event in KINDS_EVENTS['ExecutionResult']:
        if (
            step_id == event['metadata']['step_id']
            or step_id in event['metadata']['step_origin']
        ):
            touch(event)
            for attachment in event.get('attachments', []):
                name = attachment.split('_', maxsplit=2)[-1]
                if attachment in event['metadata']['attachments']:
                    metadata = event['metadata']['attachments']
                    attachments.append(
                        f'{indent}    | {name} | {metadata[attachment]["uuid"]} | {metadata[attachment].get("type", "n/a")} | )'
                    )
                else:
                    attachments.append(f'{indent}    | {name} | | |')
    if attachments:
        emit(f'{indent}??? note "Attachments ({len(attachments)})"', file=file)
        emit(f'{indent}    | Attachment | UUID | Type |', file=file)
        emit(f'{indent}    | ---------- | ---- | ---- |', file=file)
        emit('\n'.join(attachments), file=file)


def emit_workflow_attachments(file=None) -> None:
    emit('### Workflow attachments', file=file)
    attachments = []
    for event in KINDS_EVENTS['WorkflowResult']:
        for attachment in event.get('attachments', []):
            touch(event)
            name = attachment.split('_', maxsplit=2)[-1]
            if attachment in event['metadata'].get('attachments', []):
                metadata = event['metadata']['attachments']
                attachments.append(
                    f'    | {name} | {metadata[attachment].get("uuid", "n/a")} | {metadata[attachment].get("type", "n/a")} | )'
                )
            else:
                attachments.append(f'    | {name} | n/a | n/a |')
    if attachments:
        emit(f'??? note "Workflow attachments ({len(attachments)})"', file=file)
        emit('    | Attachment | UUID | Type |', file=file)
        emit('    | ---------- | ---- | ---- |', file=file)
        emit('\n'.join(attachments), file=file)
    else:
        emit('\nn/a', file=file)


def emit_dependent_events(
    step_id: str, step_origin, indent: str = '', file=None
) -> None:
    for related in IDS_EVENTS[step_id]:
        if not (metadata := related.get('metadata')):
            continue
        if not (
            step_id == metadata.get('step_id')
            and step_origin == metadata.get('step_origin')
        ):
            continue
        touch(related)
        status = (
            'failure'
            if (related['kind'] == 'ExecutionResult' and related['status'] != 0)
            else 'success'
        )
        if related['kind'] == 'Notification' and 'logs' in related['spec']:
            continue
        emit(f'{indent}??? {status} "{related["kind"]}"', file=file)
        if related['kind'] == 'ExecutionResult' and related.get('logs'):
            emit_block('text', related['logs'], indent + INDENT, file)
        elif related['kind'] == 'ProviderResult':
            emit_steps_details(
                related['steps'],
                step_origin + [step_id],
                indent=indent + INDENT,
                file=file,
            )
        emit_collapsed_yaml('Raw YAML', 'example', related, indent + INDENT, file=file)


def maybe_emit_variables_table(variables, indent='', file=None):
    if variables:
        emit(f'{indent}| Name | Value | Verbatim |', file=file)
        emit(f'{indent}| ---- | ----- | -------- |', file=file)
        for var, value in variables.items():
            if isinstance(value, str):
                emit(f'{indent}| `{var}` | `{value}` | False |', file=file)
            else:
                emit(
                    f'{indent}| `{var}` | `{value["value"]}` | {value["verbatim"]}',
                    file=file,
                )
        emit(file=file)


def emit_step_details(step, origin, index, indent='', file=None):
    step_id = step.get('id')
    if 'run' in step:
        what = step['run']
    else:
        what = step['uses']
    status = find_step_status(step_id)
    emit(f'{indent}??? {status} "{index:0>3} {format_title(what)}"', file=file)
    maybe_emit('name', step, indent + INDENT, file=file)
    maybe_emit('id', step, indent + INDENT, file=file)
    maybe_emit('if', step, indent + INDENT, file=file)
    if 'with' in step:
        for input in step['with']:
            emit(f'{indent}{INDENT}- `{input}`', file=file)
        emit(file=file)
    maybe_emit('working-directory', step, indent + INDENT, file=file)
    maybe_emit('continue-on-error', step, indent + INDENT, file=file)
    if step_id:
        emit(file=file)
        emit_dependent_events(step_id, origin, indent + INDENT, file=file)
    if not origin:
        emit_attachments_table(step_id, indent + INDENT, file=file)
    emit_collapsed_yaml('Raw YAML', 'example', step, indent + INDENT, file=file)


def maybe_find_step_id(index, job_id):
    step_ids = []
    for event in KINDS_EVENTS['ExecutionCommand']:
        if (
            event['metadata'].get('job_id') == job_id
            and event['metadata']['step_sequence_id'] != -1
        ):
            touch(event)
            origin = event['metadata']['step_origin']
            if origin:
                if origin[0] not in step_ids:
                    step_ids.append(origin[0])
            else:
                if event['metadata']['step_id'] not in step_ids:
                    step_ids.append(event['metadata']['step_id'])
    return step_ids[index - 1]


def generate_step(index, step, job_id, folder):
    """Generate a top-level step document."""
    with open(
        f'docs/workflows/{folder}/{job_id}_{index:0>3}.md', 'w', encoding='utf-8'
    ) as file:
        step_id = step.get('id') or maybe_find_step_id(index, job_id)
        if 'run' in step:
            what = step['run']
        else:
            what = step['uses']
        status = find_step_status(step_id)

        emit(f'# {index:0>3} {format_title(what)}', file=file)
        emit(f'Status: **`{status}`**', file=file)

        emit('## Metadata', file=file)
        maybe_emit('name', step, file=file)
        maybe_emit('id', step, file=file)
        maybe_emit('if', step, file=file)

        emit('## Command', file=file)

        if 'run' in step:
            emit_block('yaml', [f'run: {step["run"]}'], file=file)
        elif 'uses' in step:
            emit_block('yaml', [f'uses: {step["uses"]}'], file=file)
        if 'with' in step:
            emit('## Inputs', file=file)
            emit(file=file)
            for input in step['with']:
                emit(f'- `{input}`', file=file)
            emit(file=file)
        maybe_emit('working-directory', step, file=file)
        maybe_emit('continue-on-error', step, file=file)

        if step_id:
            emit('## Events', file=file)
            emit_dependent_events(step_id, [], file=file)
        emit_attachments_table(step_id, file=file)

        emit('## Raw YAML', file=file)
        emit_yaml(step, file=file)


########################################################################
# logs renderers

BUTTON_NONE = 0
BUTTON_LOGS = 1
BUTTON_ACCESSLOG = 2
BUTTON_COMBINEDLOGS = 3
BUTTON_COUNT = 3

LOG_BUTTONS = '''
[Logs](logs.md){ BUTTON_1 }
[Access logs](access_logs.md){ BUTTON_2 }
[Combined logs](combined_logs.md){ BUTTON_3 }
&nbsp;
[Worker logs](workers.txt){ .md-button }

[(logs.txt)](logs.txt)
[(access_logs.txt)](access_logs.txt)
[(combined_logs.txt)](combined_logs.txt)
[(workers.txt)](workers.txt)
'''

BUTTON_SELECTED = '.md-button .md-button--primary'
BUTTON_UNSELECTED = '.md-button'


def emit_log_buttons(button, alert: bool = False, file=None):
    buttons = LOG_BUTTONS
    for i in range(BUTTON_COUNT + 1):
        buttons = buttons.replace(
            f'BUTTON_{i}', BUTTON_SELECTED if i == button else BUTTON_UNSELECTED
        )
    if alert:
        buttons = buttons.replace('Worker logs', ':material-alert: Worker logs')
    emit(buttons, file=file)


def generate_log(workflow_name, title, root_name, button, key, alert):
    with open(f'{root_name}.md', 'w', encoding='utf-8') as md, open(
        f'{root_name}.txt', 'w', encoding='utf-8'
    ) as log:
        emit(f'# {title}', file=md)

        emit(workflow_name, file=md)
        emit(file=md)
        emit_log_buttons(button, alert, file=md)
        emit('## Logs', file=md)
        emit('```text', file=md)
        for event in KINDS_EVENTS['Notification']:
            if key(event):
                touch(event)
                emit(event['spec']['logs'][0], file=md)
                emit(event['spec']['logs'][0], file=log)
        emit('```', file=md)


def generate_logs(workflow_name, folder):
    """Generate logs files."""
    alert = False
    setup = set()
    teardown = set()
    with open(f'docs/workflows/{folder}/workers.txt', 'w', encoding='utf-8') as f:
        workers = [
            event for event in KINDS_EVENTS['Notification'] if 'worker' in event['spec']
        ]
        for worker in workers:
            touch(worker)
            if worker['spec']['worker']['status'] == 'setup':
                setup.add(worker['spec']['worker']['worker_id'])
            else:
                teardown.add(worker['spec']['worker']['worker_id'])
            emit('---', file=f)
            emit(yaml.dump(worker), file=f)
    if setup - teardown:
        alert = True
        print('!!! unreleased workers')
    if teardown - setup:
        alert = True
        print('!!! inconsistent workers status')
    print(len(setup), len(teardown))

    generate_log(
        workflow_name,
        'Logs',
        f'docs/workflows/{folder}/logs',
        BUTTON_LOGS,
        lambda event: is_log_notification(event) and not is_accesslog(event),
        alert,
    )
    generate_log(
        workflow_name,
        'Access logs',
        f'docs/workflows/{folder}/access_logs',
        BUTTON_ACCESSLOG,
        is_accesslog,
        alert,
    )
    generate_log(
        workflow_name,
        'Combined logs',
        f'docs/workflows/{folder}/combined_logs',
        BUTTON_COMBINEDLOGS,
        is_log_notification,
        alert,
    )
    return alert


########################################################################
# job renderers


def maybe_find_selected_channel_id(job_id: Optional[str]) -> Optional[str]:
    selected = [
        event
        for event in KINDS_EVENTS['ExecutionCommand']
        if event['metadata']['step_sequence_id'] == 0
        and event['metadata']['job_id'] == job_id
    ]
    if len(selected) == 1:
        return selected[0]['metadata']['channel_id']
    return None


def maybe_emit_selected_offer(offers, selected: Optional[str], file=None) -> None:
    """Emit selected channel if applicable."""
    if not selected:
        return
    emit('###### Selected offer', file=file)
    offer = [event for event in offers if event['metadata']['channel_id'] == selected]
    if not offer:
        emit(f'??? bug "Could not find offer with channel_id {selected}"', file=file)
        return

    metadata = offer[0]['metadata']
    emit(f'!!! tip "{metadata["channel_id"]}"', file=file)
    emit(f'    OS: `{metadata["channel_os"]}`', file=file)
    emit(file=file)
    emit(f'    Tags: {format_tags(metadata["channel_tags"])}', file=file)
    emit(file=file)


def emit_channel_request_log(requests, job_id, folder, file=None):
    """Emit channel request summary and request log."""
    if not requests:
        return

    truncate = len(requests) > MAX_REQUESTS
    with open(
        f'docs/workflows/{folder}/{job_id}_request.txt', 'w', encoding='utf-8'
    ) as log:
        for i, event in enumerate(requests):
            touch(event)
            emit('---', file=log)
            emit(yaml.dump(event), file=log)
            if truncate:
                if i == 2:
                    emit(
                        f'{INDENT}[(click here to see all requests)]({job_id}_request.txt)',
                        file=file,
                    )
                if 2 <= i < len(requests) - 2:
                    continue
            emit_collapsed_yaml(
                'ExecutionCommand',
                find_step_status(event['metadata']['step_id']),
                event,
                INDENT,
                file=file,
            )


def emit_channel_request(name: str, folder: str, file=None):
    """Emit channel request pane."""
    emit('##### Channel request', file=file)
    job_id = selected = None
    requests = [
        event
        for event in KINDS_EVENTS['ExecutionCommand']
        if event['metadata']['step_sequence_id'] == -1
        and event['metadata']['name'] == name
    ]
    offers = [
        event
        for event in KINDS_EVENTS['ExecutionResult']
        if event['metadata']['step_sequence_id'] == -1
    ]
    if requests:
        job_id = requests[0]['metadata']['job_id']
        offers = [event for event in offers if event['metadata']['job_id'] == job_id]
    else:
        offers = [event for event in offers if event['metadata']['name'] == name]
        if offers:
            job_id = offers[0]['metadata']['job_id']

    if not requests and not offers:
        status = 'quote'
    elif not requests:
        status = 'bug'
    elif not offers:
        status = 'failure'
    else:
        status = 'success'
    emit(f'??? {status} "Channel request ({len(requests)}/{len(offers)})"', file=file)

    emit_channel_request_log(requests, job_id, folder, file)
    if status == 'bug':
        emit('    ??? bug "No channel request found"', file=file)
    if status == 'failure':
        emit('    ??? failure "No channel offer received"', file=file)

    selected = maybe_find_selected_channel_id(job_id)

    for event in offers:
        touch(event)
        emit_collapsed_yaml(
            'ExecutionResult',
            'tip' if event['metadata']['channel_id'] == selected else 'quote',
            event,
            INDENT,
            file=file,
        )

    maybe_emit_selected_offer(offers, selected, file=file)
    return job_id


def maybe_emit_job_executionerrors(job_id, file=None):
    """Emit job execution error pane if needed."""
    errors = [
        event
        for event in KINDS_EVENTS['ExecutionError']
        if event['metadata'].get('job_id') == job_id
    ]
    if errors:
        emit('##### Execution errors', file=file)
        emit(f'???+ warning "Execution errors ({len(errors)})"', file=file)
        for error in errors:
            touch(error)
            if 'details' in error and 'error' in error['details']:
                msg = format_title(error['details']['error'])
            else:
                msg = 'Execution error'
            emit_collapsed_yaml(msg, 'warning', error, indent=INDENT, file=file)


def emit_job_steps(job_name: str, job, folder: str, file=None) -> Optional[str]:
    """Emit 'steps' job definition."""
    job_id = emit_channel_request(job_name, folder, file=file)

    maybe_emit_job_executionerrors(job_id, file=file)

    emit('##### Steps', file=file)
    durations = emit_steps_sumary(
        job['steps'], job_id=job_id, origin=[], folder=folder, file=file
    )

    emit('##### Channel release', file=file)
    release_requests = [
        event
        for event in KINDS_EVENTS['ExecutionCommand']
        if event['metadata'].get('job_id') == job_id
        and event['metadata'].get('step_sequence_id') == -2
    ]
    release_responses = [
        event
        for event in KINDS_EVENTS['ExecutionResult']
        if event['metadata'].get('job_id') == job_id
        and event['metadata'].get('step_sequence_id') == -2
    ]
    if not (release_requests and release_responses):
        status = 'warning'
    else:
        status = find_step_status(release_responses[0]['metadata']['step_id'])
    emit(f'??? {status} "Channel release"', file=file)
    if not release_requests:
        emit('    ??? bug "No channel release requests found"', file=file)
    for event in release_requests:
        touch(event)
        emit_collapsed_yaml(
            'ExecutionCommand', 'success', event, indent=INDENT, file=file
        )
    if not release_responses:
        emit('    ??? bug "No channel release response found"', file=file)
    for event in release_responses:
        touch(event)
        emit_collapsed_yaml(
            'ExecutionResult',
            find_step_status(event['metadata']['step_id']),
            event,
            indent=INDENT,
            file=file,
        )

    emit('##### Steps Durations', file=file)

    emit(f'??? note "Steps durations ({len(durations)})"', file=file)
    emit(f'    | Step | Elapsed | Used |', file=file)
    emit(f'    | ---- | ------- | ---- |', file=file)
    for i, (elapsed, used) in enumerate(durations, 1):
        emit(
            f'    | {i:0>3} | {format_timedelta(elapsed)} | {format_timedelta(used)} |',
            file=file,
        )

    duration_filename = f'docs/workflows/{folder}/{job_id}_steps_durations.csv'
    with open(duration_filename, 'w', encoding='utf-8') as f:
        writer = csv.writer(f, delimiter=';')
        writer.writerow(['Step', 'Elapsed', 'Used'])
        for i, (elapsed, used) in enumerate(durations, 1):
            writer.writerow([i, elapsed, used])
    emit(file=file)
    relative_filename = duration_filename.replace(f'docs/workflows/{folder}/', '')
    emit(f'    Download as CSV: [{relative_filename}]({relative_filename})', file=file)

    emit(
        f'    <canvas id="steps_durations_{job_id}" width="400" height="200"></canvas>',
        file=file,
    )
    emit(f'    <script>', file=file)
    emit(
        f'        var ctx = document.getElementById("steps_durations_{job_id}").getContext("2d");',
        file=file,
    )
    emit(f'        var myChart = new Chart(ctx, {{', file=file)
    emit(f'            type: "bar",', file=file)
    emit(f'            data: {{', file=file)
    emit(
        f'                labels: [{",".join([str(i) for i in range(1, len(durations)+1)])}],',
        file=file,
    )
    emit(f'                datasets: [{{', file=file)
    emit(f'                    label: "Elapsed",', file=file)
    emit(
        f'                    data: [{",".join([str(d[0].total_seconds()) for d in durations])}],',
        file=file,
    )
    emit(f'                    backgroundColor: [', file=file)
    emit(f'                        "rgba(255, 99, 132, 0.2)",', file=file)
    emit(f'                        "rgba(54, 162, 235, 0.2)",', file=file)
    emit(f'                        "rgba(255, 206, 86, 0.2)",', file=file)
    emit(f'                        "rgba(75, 192, 192, 0.2)",', file=file)
    emit(f'                        "rgba(153, 102, 255, 0.2)",', file=file)
    emit(f'                        "rgba(255, 159, 64, 0.2)"', file=file)
    emit(f'                    ],', file=file)
    emit(f'                    borderColor: [', file=file)
    emit(f'                        "rgba(255, 99, 132, 1)",', file=file)
    emit(f'                        "rgba(54, 162, 235, 1)",', file=file)
    emit(f'                        "rgba(255, 206, 86, 1)",', file=file)
    emit(f'                        "rgba(75, 192, 192, 1)",', file=file)
    emit(f'                        "rgba(153, 102, 255, 1)",', file=file)
    emit(f'                        "rgba(255, 159, 64, 1)"', file=file)
    emit(f'                    ],', file=file)
    emit(f'                    borderWidth: 1', file=file)
    emit(f'                }}, {{', file=file)
    emit(f'                    label: "Used",', file=file)
    emit(
        f'                    data: [{",".join([str(d[1].total_seconds()) for d in durations])}],',
        file=file,
    )
    emit(f'                    backgroundColor: [', file=file)
    emit(f'                        "rgba(255, 99, 132, 0.2)",', file=file)
    emit(f'                        "rgba(54, 162, 235, 0.2)",', file=file)
    emit(f'                        "rgba(255, 206, 86, 0.2)",', file=file)
    emit(f'                        "rgba(75, 192, 192, 0.2)",', file=file)
    emit(f'                        "rgba(153, 102, 255, 0.2)",', file=file)
    emit(f'                        "rgba(255, 159, 64, 0.2)"', file=file)
    emit(f'                    ],', file=file)
    emit(f'                    borderColor: [', file=file)
    emit(f'                        "rgba(255, 99, 132, 1)",', file=file)
    emit(f'                        "rgba(54, 162, 235, 1)",', file=file)
    emit(f'                        "rgba(255, 206, 86, 1)",', file=file)
    emit(f'                        "rgba(75, 192, 192, 1)",', file=file)
    emit(f'                        "rgba(153, 102, 255, 1)",', file=file)
    emit(f'                        "rgba(255, 159, 64, 1)"', file=file)
    emit(f'                    ],', file=file)
    emit(f'                    borderWidth: 1', file=file)
    emit(f'                }}]', file=file)
    emit(f'            }},', file=file)
    emit(f'            options: {{', file=file)
    emit(f'                scales: {{', file=file)
    emit(f'                    yAxes: [{{', file=file)
    emit(f'                        ticks: {{', file=file)
    emit(f'                            beginAtZero: true', file=file)
    emit(f'                        }}', file=file)
    emit(f'                    }}]', file=file)
    emit(f'                }}', file=file)
    emit(f'            }}', file=file)
    emit(f'        }});', file=file)
    emit(f'    </script>', file=file)
    emit(file=file)

    return job_id


def emit_job_generator(job_name: str, job, folder: str, file=None) -> Optional[str]:
    """Emit 'generator' job definition."""
    emit('##### Command', file=file)

    emit(f'```yaml\ngenerator: {job["generator"]}\n```', file=file)
    if job.get('with'):
        emit('##### Inputs', file=file)
        emit_yaml(job['with'], file=file)
    emit(file=file)

    generators = [
        event
        for event in KINDS_EVENTS['GeneratorCommand']
        if event['metadata']['name'] == job_name and not event['metadata']['job_origin']
    ]
    if len(generators) != 1:
        emit(
            '!!! bug "Was expecting one `GeneratorCommand` message, got %d"'
            % len(generators),
            file=file,
        )
    job_id = generators[0]['metadata']['job_id']
    maybe_emit_job_executionerrors(job_id, file=file)

    emit('##### Query', file=file)
    for generator in generators:
        touch(generator)
        emit_collapsed_yaml('GeneratorCommand', 'success', generator, file=file)

    responses = [
        event
        for event in KINDS_EVENTS['GeneratorResult']
        if event['metadata']['job_id'] == job_id
    ]
    emit('##### Responses', file=file)
    for i, event in enumerate(responses):
        touch(event)
        emit_collapsed_yaml(
            'GeneratorResult',
            'success' if i == 0 else 'quote',
            event,
            file=file,
        )
        if i == 0:
            for nested_job in event['jobs']:
                GENERATED_JOBS[nested_job] = event['jobs'][nested_job]
    return job_id


def emit_job_notifications(job_id, file=None):
    """Emit job notification pane."""
    notifications = [
        event
        for event in KINDS_EVENTS['Notification']
        if event['metadata'].get('job_id') == job_id
        and not event['metadata'].get('step_id')
    ]

    emit('##### Notifications', file=file)
    emit(f'??? note "Job notifications ({len(notifications)})"', file=file)
    for i, event in enumerate(notifications, 1):
        touch(event)
        emit_collapsed_yaml(
            f'{i:0>3} Notification ({event["metadata"]["name"]})',
            'note',
            event,
            indent=INDENT,
            file=file,
        )


def emit_jobs(jobs, folder, file):
    """Emit all jobs definitions."""
    for job_name, job in jobs.items():
        emit(f'#### {job_name}', file=file)

        emit(f'Required tags: {format_tags(job["runs-on"])}', file=file)
        maybe_emit('if', job, file=file)
        maybe_emit('continue-on-error', job, file=file)
        maybe_emit('needs', job, file=file)

        if 'steps' in job:
            job_id = emit_job_steps(job_name, job, folder, file=file)
        elif 'generator' in job:
            job_id = emit_job_generator(job_name, job, folder, file=file)
        else:
            emit_collapsed_yaml(
                'Unknown job format: neither a generator nor a steps job',
                'bug',
                job,
                file=file,
            )
            continue
        if job_id:
            emit_job_notifications(job_id, file=file)
        emit('##### Raw YAML', file=file)
        emit_collapsed_yaml('Raw YAML', 'example', job, file=file)


########################################################################
# workflow pane renderers


def maybe_emit_workflow_duration(workflow, file=None):
    """
    Channel usage time may (slightly) exceed elapsed running time, due
    to the fact that elapsed running time ends with WorkflowCanceled or
    WorkflowCompleted, while channel usage time includs channel
    cleanup duration.  (WorkflowXxx events are emitted before channel
    cleanup.)
    """
    start = as_datetime(workflow)
    end = datetime(9999, 12, 31)
    for event in KINDS_EVENTS['WorkflowCanceled']:
        end = min(end, as_datetime(event))
    for event in KINDS_EVENTS['WorkflowCompleted']:
        end = min(end, as_datetime(event))

    if end != datetime(9999, 12, 31):
        emit(
            f'Elapsed running time: **`{format_timedelta(end-start)}`** (logical)',
            file=file,
        )
        channel_time = timedelta(0)
        jobs_starts = [
            event
            for event in KINDS_EVENTS['ExecutionCommand']
            if event['metadata']['step_sequence_id'] == 0
        ]
        jobs_ends = [
            event
            for event in KINDS_EVENTS['ExecutionResult']
            if event['metadata']['step_sequence_id'] == -2
        ]
        starts = {event['metadata']['job_id'] for event in jobs_starts}
        ends = {event['metadata']['job_id'] for event in jobs_ends}
        if (
            len(jobs_starts) == len(jobs_ends)
            and len(starts) == len(ends)
            and len(jobs_starts) == len(starts)
        ):
            for job_start in jobs_starts:
                job_ends = [
                    event
                    for event in jobs_ends
                    if event['metadata']['job_id'] == job_start['metadata']['job_id']
                ]
                if len(job_ends) == 1:
                    channel_time += as_datetime(job_ends[0]) - as_datetime(job_start)
            emit(
                f'<br/>Channel usage time: **`{format_timedelta(channel_time)}`**',
                file=file,
            )
        else:
            emit('<br/>Channel usage time: not available (mismatched data)')


def emit_workflow_status(workflow, status, file=None):
    """Emit workflow status pane."""
    emit(f'Status: **`{status}`**', file=file)
    emit(file=file)

    maybe_emit_workflow_duration(workflow, file=file)

    for event in KINDS_EVENTS['WorkflowCanceled']:
        touch(event)
        emit_collapsed_yaml('WorkflowCanceled', 'warning', event, file=file)
    for event in KINDS_EVENTS['WorkflowCompleted']:
        touch(event)
        emit_collapsed_yaml('WorkflowCompleted', 'quote', event, file=file)


def emit_workflow_executionsequence(workflow, file=None):
    """Emit workflow execution sequence."""
    emit('### Jobs execution sequence', file=file)
    lhs = '[*]'
    emit('```mermaid', file=file)
    emit('stateDiagram', file=file)
    emit('  direction LR', file=file)
    for job_name in workflow['jobs']:
        emit(f'   {lhs} --> {job_name.replace("-", "#45;")}', file=file)
        lhs = job_name.replace("-", "#45;")
    generators = [event for event in KINDS_EVENTS['GeneratorResult']]
    for job_name, job in workflow['jobs'].items():
        if 'generator' in job:
            emit(f'    state {job_name.replace("-", "#45;")} {{', file=file)
            emit('      direction TB', file=file)
            for event in generators:
                if (
                    event['metadata']['name'] == job_name
                    and not event['metadata']['job_origin']
                ):
                    for generated in event['jobs']:
                        emit(f'      {generated.replace("-", "#45;")}', file=file)
            emit('    }', file=file)
    emit(f'  {lhs} --> [*]', file=file)
    emit('```', file=file)
    emit(file=file)


def emit_workflow_logs(workflow, folder, file=None):
    """Emit workflow logs pane."""
    emit('### Logs', file=file)
    alert = generate_logs(workflow['metadata']['name'], folder)
    emit_log_buttons(BUTTON_NONE, alert, file=file)


def emit_workflow_notifications(file=None):
    """Emit workflow notifications pane."""
    emit('### Workflow notifications', file=file)
    notifications = [
        event
        for event in KINDS_EVENTS['Notification']
        if is_workflow_notification(event)
        and not is_log_notification(event)
        and not is_worker_notification(event)
    ]
    if not notifications:
        emit('n/a', file=file)
    else:
        emit(f'??? note "Workflow notifications ({len(notifications)})"', file=file)
    for i, notification in enumerate(notifications, 1):
        touch(notification)
        emit_collapsed_yaml(
            f'{i:0>3} Notification ({notification["metadata"]["name"]})',
            'note',
            notification,
            INDENT,
            file=file,
        )


def emit_workflow_metadata(workflow, file=None):
    """Emit workflow metadata pane."""
    emit('### Metadata', file=file)

    emit(f'Name: `{workflow["metadata"]["name"]}`', file=file)
    emit(f'<br/>Namespace: `{workflow["metadata"]["namespace"]}`', file=file)
    emit(
        f'<br/>Reception date: `{workflow["metadata"]["creationTimestamp"]}`', file=file
    )

    emit_collapsed_yaml('Raw metadata', 'info', workflow['metadata'], file=file)


def emit_workflow_resources(workflow, file=None):
    """Emit workflow resources pane."""
    emit('### Resources', file=file)

    if 'resources' in workflow:
        emit_yaml(workflow['resources'], file=file)
    else:
        emit('n/a', file=file)


def emit_workflow_hooks(workflow, file=None):
    """Emit workflow hooks pane."""
    emit('### Hooks', file=file)

    if 'hooks' in workflow:
        emit_yaml(workflow['hooks'], file=file)
    else:
        emit('n/a', file=file)


def emit_workflow_variables(workflow, file=None):
    """Emit workflow variables pane."""
    emit('### Variables', file=file)

    if 'variables' not in workflow:
        emit('n/a', file=file)
    else:
        maybe_emit_variables_table(workflow.get('variables'), file=file)


def emit_workflow_jobs(workflow, folder, file=None):
    """Emit workflow jobs pane."""
    emit('### Jobs', file=file)

    emit_jobs(workflow['jobs'], folder=folder, file=file)
    if GENERATED_JOBS:
        emit('### Generated jobs', file=file)
        emit(file=file)
        emit_jobs(GENERATED_JOBS, folder=folder, file=file)


def emit_workflow_raw(workflow, file=None):
    """Emit workflow raw YAML pane."""
    emit('### Raw YAML', file=file)

    emit_collapsed_yaml('Raw YAML', 'example', workflow, file=file)


def generate_workflow(events, folder):
    """Generate a workflow document."""
    with open(f'docs/workflows/{folder}/workflow.md', 'w', encoding='utf-8') as file:
        workflow = KINDS_EVENTS['Workflow'][0]
        touch(workflow)

        emit(f'# {workflow["metadata"]["name"]}', file=file)
        emit('## Workflow results', file=file)

        emit_workflow_status(workflow, events['status'], file=file)
        emit_workflow_logs(workflow, folder, file=file)
        emit_workflow_executionsequence(workflow, file=file)
        emit_workflow_attachments(file=file)
        emit_workflow_notifications(file=file)

        emit('## Workflow details', file=file)

        emit_workflow_metadata(workflow, file=file)
        emit_workflow_resources(workflow, file=file)
        emit_workflow_hooks(workflow, file=file)
        emit_workflow_variables(workflow, file=file)
        emit_workflow_jobs(workflow, folder, file=file)
        emit_workflow_raw(workflow, file=file)


########################################################################


def _parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='OpenTestFactory Dashboard Manager')
    parser.add_argument(
        '--version',
        action='version',
        version=f'OpenTestFactory Dashboard Manager version {VERSION}.',
    )
    parser.add_argument(
        '--folder',
        help='Target folder (relative, may contain more than one part).',
        required=True,
    )
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        '--load',
        help='A JSON or YAML file containing events.',
    )
    group.add_argument(
        '--unload',
        help='Remove folder.',
        action='store_true',
    )
    parser.add_argument(
        '--overwrite',
        help='Whether to replace possibly existing content (only used by --load).',
        action='store_true',
    )

    return parser.parse_args()


def unload_folder(args):
    if previous := os.listdir(f'docs/workflows/{args.folder}'):
        for entry in previous:
            os.unlink(f'docs/workflows/{args.folder}/{entry}')
    try:
        with open('mkdocs.yml', 'r', encoding='utf-8') as file:
            mkdocs = file.readlines()
    except Exception as err:
        logging.error('Could not read "mkdocs.yml": %s.', err)
        sys.exit(2)

    try:
        with open('mkdocs.yml', 'w', encoding='utf-8') as file:
            for line in mkdocs:
                if line.strip().startswith(f'- {args.folder}:'):
                    continue
                file.write(line)
    except Exception as err:
        logging.error('Could not update "mkdocs.yml": %s.', err)
        sys.exit(2)

    print(f'Folder "{args.folder}" removed.')


def load_folder(args):
    logging.info('Reading event log "%s".', args.load)
    try:
        with open(
            args.load, 'r', encoding='utf-8'
        ) if args.load != '-' else sys.stdin as f:
            events = yaml.safe_load(f)
    except Exception as err:
        logging.error('Could not read event log: %s.', err)
        sys.exit(2)

    for event in events['items']:
        KINDS_EVENTS[event['kind']].append(event)
    for event in events['items']:
        if not (metadata := event.get('metadata')):
            continue
        if 'job_id' in metadata:
            IDS_EVENTS[metadata['job_id']].append(event)
        if 'step_id' in metadata:
            IDS_EVENTS[metadata['step_id']].append(event)

    os.makedirs(f'docs/workflows/{args.folder}', exist_ok=True)
    if previous := os.listdir(f'docs/workflows/{args.folder}'):
        if not args.overwrite:
            print(
                f'Target directory "{args.folder}" not empty, aborting.  Use "--overwrite" if you want to replace existing workflow.'
            )
            sys.exit(1)
        print(
            f'Option "--overwrite" specified, overwriting existing content in "{args.folder}".'
        )
        for entry in previous:
            os.unlink(f'docs/workflows/{args.folder}/{entry}')

    generate_workflow(events, args.folder.strip('/'))

    with open('mkdocs.yml', 'r', encoding='utf-8') as file:
        mkdocs = file.readlines()
    with open('mkdocs.yml', 'w', encoding='utf-8') as file:
        for line in mkdocs:
            file.write(line)
            if not args.overwrite or not previous:
                if line.strip().startswith('- Workflows:'):
                    file.write(
                        f"    - {args.folder}: 'workflows/{args.folder}/workflow.md'\n"
                    )
    print(len(REFERENCED_EVENTS), 'out of', sum(len(_) for _ in KINDS_EVENTS.values()))
    for events in KINDS_EVENTS.values():
        for event in events:
            if id(event) not in REFERENCED_EVENTS:
                print(event)


def main():
    args = _parse_args()
    if args.unload:
        unload_folder(args)
    if args.load:
        load_folder(args)


if __name__ == '__main__':
    main()
